from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.
class Spline(models.Model):
    name = models.TextField(unique=True)


class TemperatureSetting(models.Model):
    temperature = models.DecimalField(decimal_places=2, max_digits=4)
    time = models.TimeField()
    spline = models.ForeignKey(Spline, on_delete=models.CASCADE)


class DaySpline(models.Model):
    weekday = models.IntegerField(default=0, validators=[MaxValueValidator(6), MinValueValidator(0)], unique=True)
    spline = models.ForeignKey(Spline, on_delete=models.CASCADE)
