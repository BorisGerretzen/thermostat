from django.contrib import admin
from django.urls import path, include, register_converter
from . import converters, views

register_converter(converters.FloatUrlParameterConverter, 'float')

urlpatterns = [
    path('set/<float:temperature>', views.set_temp, name='set_temp'),
    path('get/', views.get_temp, name="get_temp"),
    path('splines/new', views.spline_new, name="spline_new"),
]