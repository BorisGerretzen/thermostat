from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(TemperatureSetting)
admin.site.register(Spline)
admin.site.register(DaySpline)