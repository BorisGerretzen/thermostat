import datetime

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseBadRequest, HttpResponse
from django.shortcuts import render
from .forms import NewSplineForm
from .models import Spline, TemperatureSetting
from django.core.cache import caches


# Create your views here.
def set_temp(request, temperature):
    cache = caches['override']
    cache.set('temperature', temperature, 3600)
    print(f"Setting temperature to {temperature}")
    return HttpResponse("ok")


def get_temp(request):
    cache = caches['override']
    return HttpResponse(cache.get('temperature'))


def spline_new(request):
    if request.method == "POST":
        form = NewSplineForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data.get('name')
            if Spline.objects.filter(name=name).exists():
                return HttpResponseBadRequest(f"Name '{name}' already exists")
            spline = Spline(name=name)
            spline.save()
            ts = TemperatureSetting(temperature=20, spline=spline, time=datetime.time())
            ts.save()
            return HttpResponse("ok")
