import os
import sys
import threading

from prometheus_client import Gauge, start_http_server, Enum

temperature_measurement = Gauge('temperature', 'The measured temperature at the thermostat')
temperature_setting = Gauge('temperature_set', 'The temperature set at the thermostat, either override or schedule')
temperature_override = Gauge('temperature_override', 'The temperature set by the override, 0 if override not set')
temperature_schedule = Gauge('temperature_schedule', 'The temperature set by the schedule, 0 if no schedule')
heating_status = Enum('heating_status', 'Whether the thermostat is instructing the boiler to heat or not', states=['heating', 'off'])

if 'runserver' in sys.argv and os.environ.get('RUN_MAIN', None) != 'true':
    t = threading.Thread(target=start_http_server, args=(8001,))
    t.start()