from django.db import models
from django.db.models.signals import post_save
import datalogger.metrics as metrics


# Create your models here.
class TemperatureMeasurement(models.Model):
    temperature = models.DecimalField(decimal_places=2, max_digits=4)


# def on_temperature_measurement_save(sender, instance, **kwargs):
#     if kwargs['created']:
#         # print(instance.temperature)
#         metrics.temperature_measurement.set(instance.temperature)
#         print(metrics.temperature_measurement)
#
#
# post_save.connect(on_temperature_measurement_save, sender=TemperatureMeasurement)
