import os
import sys
from django.apps import AppConfig


class DataloggerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'datalogger'

    def ready(self):
        if 'runserver' in sys.argv and os.environ.get('RUN_MAIN', None) != 'true' and True:
            import datalogger.updater as updater
            updater.start()
            updater.run_once()
