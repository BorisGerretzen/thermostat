from apscheduler.schedulers.background import BackgroundScheduler

from datalogger.logger import logger


def start():
    scheduler = BackgroundScheduler({'apscheduler.timezone': 'Europe/Amsterdam'})
    scheduler.add_job(logger.store_data, trigger='interval', seconds=5)
    scheduler.start()


def run_once():
    logger.store_data()
