import random
from datalogger.models import TemperatureMeasurement
import datalogger.metrics as metrics


class Logger:
    def __init__(self):
        self.last_temp = 19
        self.heat = False

    def store_data(self):
        data = self.get_data()
        measurement = TemperatureMeasurement(temperature=data)
        measurement.save()
        metrics.temperature_measurement.set(data)

    def get_data(self):
        if self.heat:
            self.last_temp += random.random()*0.01
        else:
            self.last_temp -= random.random()*0.01
        return self.last_temp


logger = Logger()
