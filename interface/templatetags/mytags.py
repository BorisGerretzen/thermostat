import hashlib

from django import template
register = template.Library()


@register.filter
def unique(value):
    m = hashlib.md5()
    m.update(value.encode())
    return m.hexdigest()[0:12]

@register.filter
def num_to_day(value):
    days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
    return days[int(value)]
