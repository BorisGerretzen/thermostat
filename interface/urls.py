from django.contrib import admin
from django.urls import path, include
import interface.views as views

urlpatterns = [
    path('', views.index, name='index'),
    path('splines/', views.splines, name='splines')
]