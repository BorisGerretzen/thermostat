import datetime
import json
import math

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder
from django.shortcuts import render

from api.models import Spline, TemperatureSetting, DaySpline
from .forms import DaysForm
from api.forms import NewSplineForm


# Create your views here.
@login_required
def index(request):
    return render(request=request,
                  template_name="interface/index.html")


@login_required
def splines(request):
    # If form submitted
    if request.method == 'POST':
        formDays = DaysForm(request.POST)
        splines_post(request)
    else:
        formDays = DaysForm()
    formNewSpline = NewSplineForm()

    # Create a list containing all the settings of the spline
    spline_list = []
    for spline in Spline.objects.all():
        settings = TemperatureSetting.objects.filter(spline=spline).values_list('time', 'temperature')
        days = DaySpline.objects.filter(spline=spline).values_list('weekday', flat=True)
        spline_list.append([spline.name, json.dumps(list(settings), cls=DjangoJSONEncoder),
                            json.dumps(list(days), cls=DjangoJSONEncoder)])

    return render(request=request,
                  template_name="interface/splines.html",
                  context={'splines': spline_list,
                           'formDays': formDays,
                           'formNewSpline': formNewSpline})


def splines_post(request):
    form = DaysForm(request.POST)
    if form.is_valid():
        days = form.cleaned_data.get('days')

        # Get the spline object the form is targeting
        spline = Spline.objects.filter(name=form.cleaned_data.get('spline')).first()
        day_splines = DaySpline.objects.all()

        # Check if spline exists
        if spline is None:
            messages.error(request, 'That spline does not exist.')
            return

        # Check if days are removed from the schedule
        # We dont want users to remove from schedule, only replace from other schedules.
        spline_days = list(day_splines.filter(spline=spline).values_list('weekday', flat=True))
        days = [int(day) for day in days]
        diff = list(set(spline_days) - set(days))
        if len(diff) > 0:
            messages.error(request, 'Do not remove days, replace them with another spline.')
            return

        # Update
        for day in days:
            if not day_splines.filter(weekday=day).first() is None:
                day_splines.filter(weekday=day).update(spline=spline)
            else:
                day_spline = DaySpline(weekday=day, spline=spline)
                day_spline.save()

        # Check if there is spline data
        spline_data = form.cleaned_data.get('spline_data')
        if spline_data:
            spline_data = json.loads(spline_data)
            TemperatureSetting.objects.filter(spline=spline).delete()
            for data in spline_data:
                hour = math.floor(data[0])
                minute = round(60 * (data[0] - hour))
                temp_setting = TemperatureSetting(spline=spline,
                                                  temperature=data[1],
                                                  time=datetime.time(hour=hour, minute=minute))
                temp_setting.save()
        messages.success(request, "Spline and days saved")
    else:
        messages.error(request, 'Invalid form')
