from django import forms


class DaysForm(forms.Form):
    days_options = (
        ('0', "Monday"),
        ('1', "Tuesday"),
        ('2', "Wednesday"),
        ('3', "Thursday"),
        ('4', "Friday"),
        ('5', "Saturday"),
        ('6', "Sunday"),
    )
    days = forms.MultipleChoiceField(choices=days_options, widget=forms.CheckboxSelectMultiple, label='', required=False)
    spline = forms.CharField(widget=forms.HiddenInput(), required=True)
    spline_data = forms.CharField(widget=forms.HiddenInput(), required=False)
