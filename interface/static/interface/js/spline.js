function spline(selector, args) {
    let pixelsPerStepY = args.height / ((args.maxY - args.minY) / args.stepY);
    let pixelsPerStepX = args.width / ((args.maxX - args.minX) / args.stepX);
    let firstLineX =  0;
    let lastLineY = 0;
    let points = [];
    args.data.forEach(data => {
        points.push([data[0]/args.stepX*pixelsPerStepX, height-((data[1]/args.stepY-args.minY/args.stepY)*pixelsPerStepY)]);
    });
    points.push([args.width + 100, args.height / 2]);
    let dragged = null,
        selected = points[0];
    let line = d3.svg.line();
    line.interpolate("step-after")
    let svg = d3.select(selector).append("svg")
        .attr("width", args.width)
        .attr("height", args.height)
        .attr("tabindex", 1);

    svg.append("rect")
        .attr("width", args.width)
        .attr("height", args.height)
        .on("mousedown", mousedown)
        .on("touchstart", mousedown);

    // Draw horizontal gridlines
    let currentStep = 0;
    while (currentStep * pixelsPerStepY < args.height) {
        let coordinate = pixelsPerStepY * currentStep;
        if (args.height - coordinate > args.gridStartY && currentStep % args.gridModuloY === 0) {
            svg.append("line")
                .attr("x1", args.gridStartX)
                .attr("y1", coordinate)
                .attr("x2", args.width)
                .attr("y2", coordinate)
                .attr("shape-rendering", "crispEdges")
                .style("stroke-dasharray", ("1, 1"));
            lastLineY = coordinate;
        }
        if (currentStep % args.textModuloY === 0 && coordinate !== 0) {
            svg.append("text")
                .attr("x", 5)
                .attr("y", coordinate + args.fontSize / 4)
                .attr("font-size", args.fontSize)
                .text((args.height - currentStep * pixelsPerStepY) / args.height * (args.maxY - args.minY) + args.minY);
        }
        currentStep++;
    }

    // Draw vertical gridlines
    currentStep = 0;
    while (currentStep * pixelsPerStepX < args.width) {
        let coordinate = pixelsPerStepX * currentStep;
        if (coordinate >= args.gridStartX && currentStep % args.gridModuloX === 0) {
            svg.append("line")
                .attr("x1", coordinate)
                .attr("y1", 0)
                .attr("x2", coordinate)
                .attr("y2", args.height - args.gridStartY)
                .attr("shape-rendering", "crispEdges")
                .style("stroke-dasharray", ("1, 1"));
            if(firstLineX === 0) {
                firstLineX = coordinate
            }
        }

        if (currentStep % args.textModuloX === 0 && coordinate !== 0) {
            let text = svg.append("text")
                .attr("x", coordinate)
                .attr("y", args.height - 5)
                .attr("font-size", args.fontSize)
                .text(Math.round((currentStep * pixelsPerStepX) / args.width * (args.maxX - args.minX) + args.minX) + ":00");
            bbox = text[0][0].getBBox();
            text.attr("x", coordinate - bbox.width / 2);
        }
        currentStep++;
    }

    function pixelsToCoordinates(x, y) {
        let xNew = Math.round(10*(x/pixelsPerStepX)*args.stepX)/10;
        let yNew = Math.round(10*(args.maxY-(y/pixelsPerStepY)*args.stepY))/10;
        return [xNew, yNew];
    }

    svg.append("path")
        .datum(points)
        .attr("class", "line")
        .call(redraw);

    d3.select(window)
        .on("mousemove", mousemove)
        .on("mouseup", mouseup)
        .on("keydown", keydown)
        .on("touchleave", mouseup)
        .on("touchend", mouseup)
        .on("touchcancel", mouseup)
        .on("touchmove", mousemove);


    function redraw() {
        points.sort((a, b) => {
            return a[0] - b[0];
        });
        svg.select("path").attr("d", line);

        let circle = svg.selectAll("circle")
            .data(points, function (d) {
                return d;
            });

        circle.enter().append("circle")
            .attr("r", 1e-6)
            .on("mousedown", function (d) {
                selected = dragged = d;
                redraw();
            })
            .on("touchstart", function (d) {
                selected = dragged = d;
                redraw();
            })
            .transition()
            .duration(750)
            .ease("elastic")
            .attr("r", 6.5);

        circle
            .classed("selected", function (d) {
                return d === selected;
            })
            .attr("cx", function (d) {
                return d[0];
            })
            .attr("cy", function (d) {
                return d[1];
            });

        circle.exit().remove();

        if (d3.event) {
            d3.event.preventDefault();
            d3.event.stopPropagation();
        }
    }

    function mousedown() {
        points.push(selected = dragged = d3.mouse(svg.node()));
        redraw();
    }

    function mousemove() {
        if (!dragged) return;
        var m = d3.mouse(svg.node());
        if (dragged !== points[0] && dragged !== points[points.length - 1]) {
            dragged[0] = Math.round(Math.max(firstLineX, Math.min(args.width, m[0])) / pixelsPerStepX) * pixelsPerStepX;
        }
        dragged[1] = Math.round(Math.max(0, Math.min(lastLineY, m[1])) / pixelsPerStepY) * pixelsPerStepY;
        pixelsToCoordinates(dragged[0], dragged[1]);
        redraw();
    }

    function mouseup() {
        if (!dragged) return;
        mousemove();
        xys = [];
        points.forEach(point => {
            xys.push(pixelsToCoordinates(point[0], point[1]));
        });
        xys.pop();
        args.movementCallback(xys);
        dragged = null;
    }

    function keydown() {
        if (!selected) return;
        switch (d3.event.keyCode) {
            case 8: // backspace
            case 46: { // delete
                var i = points.indexOf(selected);
                if (i !== 0 && i !== points.length) {
                    points.splice(i, 1);
                    selected = points.length ? points[i > 0 ? i - 1 : 0] : null;
                    redraw();
                }
                break;
            }
        }
    }
    return pixelsToCoordinates;
}