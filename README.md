# Thermostat
My house has an archaic thermostat, and I do not want to pay big $$ for a smart one.\

## Setup
Assuming you have cloned the repository:
- Create file ``.env``, add keys ``SECRET_KEY, POSTGRES_USER, POSTGRES_PASSWORD``, ``KEY="value"``
- Create file ``postgres.env`` add keys ``POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_DB`` , ``KEY=value``
- ``docker-compose up``