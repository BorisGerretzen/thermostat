# syntax=docker/dockerfile:1
FROM python:3.10.0-slim-buster
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
EXPOSE 8000
EXPOSE 8001
RUN apt-get update && apt-get -y install libpq-dev gcc
RUN pip install -r requirements.txt
COPY . /code/
