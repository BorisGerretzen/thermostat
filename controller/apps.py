import os
import sys
import time

from django.apps import AppConfig


class ControllerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'controller'
    if 'runserver' in sys.argv and os.environ.get('RUN_MAIN', None) != 'true' and True:
        import controller.updater as updater
        updater.start()
        updater.run_once()

def worker():
    while True:
        time.sleep(10)