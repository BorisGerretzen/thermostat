from apscheduler.schedulers.background import BackgroundScheduler
import controller.temperature_controller as temp

def start():
    scheduler = BackgroundScheduler({'apscheduler.timezone': 'Europe/Amsterdam'})
    scheduler.add_job(temp.control_thermostat(), trigger='interval', seconds=5)
    scheduler.start()


def run_once():
    temp.control_thermostat()
