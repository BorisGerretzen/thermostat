import datetime

import requests
from decouple import config
from django.core.cache import caches

import datalogger.metrics as metrics
from api.models import TemperatureSetting, DaySpline
from datalogger.models import TemperatureMeasurement
from datalogger.logger import logger

def get_temperature():
    last_measurement = TemperatureMeasurement.objects.all().last()
    return last_measurement.temperature


def get_target_temperature():
    cache = caches['override']

    # If there is an active override, return that.
    if not cache.get('temperature') is None:
        temp = cache.get('temperature')
        metrics.temperature_override.set(temp)
        metrics.temperature_setting.set(temp)
        return temp

    # Get the temperature spline corresponding to today
    current_weekday = datetime.datetime.today().weekday
    dayspline = DaySpline.objects.filter(weekday=current_weekday)
    if not dayspline.exists():
        metrics.temperature_schedule.set(0)
        print("No spline for today")
        return 10
    current_spline = dayspline.spline

    # Get the setting for this moment in the day
    # Loop over all temperature settings until the time > current time
    settings_today = TemperatureSetting.objects.filter(spline=current_spline)
    target_node = None
    for setting in settings_today:
        if datetime.datetime.now().time() <= setting.time:
            break
        else:
            target_node = setting

    if not target_node is None:
        metrics.temperature_schedule.set(target_node.temperature)
        metrics.temperature_setting.set(target_node.temperature)
        return target_node.temperature

    raise Exception("No current temperature setting found.")


def needs_heating():
    return get_target_temperature() - get_temperature() < 1.0


def control_thermostat():
    if needs_heating():
        target_url = config('TARGET_URL_HEATING')
        logger.heat = True
        metrics.heating_status.state('heating')
    else:
        target_url = config('TARGET_URL_STOP')
        logger.heat = False
        metrics.heating_status.state('off')
    r = requests.get(target_url)
    if r.status_code != 200:
        print("Warning: could not connect to heating server")
